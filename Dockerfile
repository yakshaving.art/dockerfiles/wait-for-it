FROM alpine:3

RUN apk add --no-cache bash

COPY wait-for-it /usr/bin

ENTRYPOINT [ "/usr/bin/wait-for-it" ]
